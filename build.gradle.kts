plugins {
    kotlin("jvm")
    maven
}

repositories {
    jcenter()
    maven { setUrl("https://jitpack.io/") }
}

dependencies {
    val karbonpowered_commons_version: String by project

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.github.karbonpowered", "commons", karbonpowered_commons_version)
}
