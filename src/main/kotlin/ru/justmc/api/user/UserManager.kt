package ru.justmc.api.user

import java.util.*

interface UserManager {
    fun getUser(uniqueId: UUID): User

    fun getUser(name: String): User

    companion object : UserManager by UserManager.INSTANCE {
        lateinit var INSTANCE: UserManager
    }
}