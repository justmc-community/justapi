package ru.justmc.api.user

import com.github.karbonpowered.commons.Identifiable
import com.github.karbonpowered.commons.Nameable
import ru.justmc.api.session.Session
import java.util.*

interface User : Identifiable, Nameable {
    override val uniqueId: UUID
    override val name: String
    var locale: Locale
    val session: Session?
}