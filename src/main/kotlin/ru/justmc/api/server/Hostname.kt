package ru.justmc.api.server

interface Hostname {
    val value: String
}