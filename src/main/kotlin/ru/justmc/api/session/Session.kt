package ru.justmc.api.session

import com.github.karbonpowered.commons.Identifiable
import ru.justmc.api.server.Hostname
import ru.justmc.api.user.User
import java.net.InetAddress
import java.time.Instant
import java.util.*

interface Session : Identifiable {
    override val uniqueId: UUID
    val user: User
    val hostname: Hostname
    val address: InetAddress
    val startTime: Instant
    var endTime: Instant?
    var lastServer: String?
    var disconnectionReason: String?
    var authorized: Boolean
}