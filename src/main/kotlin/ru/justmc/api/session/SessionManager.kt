package ru.justmc.api.session

import ru.justmc.api.user.User

interface SessionManager {
    fun getLastSession(user: User): Session?

    fun createNewSession(user: User): Session

    fun saveSession(session: Session)

    companion object : SessionManager by SessionManager.INSTANCE {
        lateinit var INSTANCE: SessionManager
    }
}